using Serialization
using StatsBase
using Statistics
using DataFrames
using LaTeXStrings
using StatsPlots
includet("lib/commondefs.jl")
pyplot()

function plot_sfd_df_direct(df, outfname)
    df = filter(x -> x.nwalkers == 7, df) |> sort
    ylims = (0.45, 0.96)    # May need to change this depending on sim size

    # DF currently contains one row per particleid + nsites, need to aggregate
    # data by taking mean over particle ids

    gdf = groupby(df, :nsites)

    # Do for remaining columns
    df = combine(gdf, :true_bits => mean, :m0_bits => mean, :m1_bits => mean,
        :m2_bits => mean, :m3_bits => mean, :m0_plugin => mean, :m1_plugin => mean,
        :m2_plugin => mean, :m3_plugin => mean, :nsites => first, :kind => first,
        :nsteps => first, renamecols=false)

    nsites = df.nsites
    direct_bps = df.true_bits ./ df.nsteps
    markov_bps = df.m1_bits ./ df.nsteps
    m2_bps = df.m2_bits ./ df.nsteps
    m3_bps = df.m3_bits ./ df.nsteps

    correction = markov_bps .- df.m1_plugin
    direct_bps = direct_bps .- correction
    markov_bps = markov_bps .- correction
    m2_bps = m2_bps .- correction
    m3_bps = m3_bps .- correction

    p1 = plot(dpi=1200, size=(700, 350), legend=:bottomright,
        xlabel=L"$R$", ylabel=bitrate_label,
        xticks=8:2:maximum(nsites), legendfontsize=16, xlabelfontsize=16,
        ylabelfontsize=16, xtickfontsize=14, ytickfontsize=14)
    plot!(nsites, markov_bps, label=L"$h^{(1)}$",
        color=markov1_col, markershape=markov1_shape, linewidth=1.5,
        markerstrokewidth=0.5, markersize=8)
    plot!(nsites, m2_bps, label=L"$h^{(2)}$",
        color=markov2_col, markershape=markov2_shape, linewidth=1.5,
        markerstrokewidth=0.5, markersize=8)
    plot!(nsites, m3_bps, label=L"$h^{(3)}$",
        color=other1_col, markershape=other1_shape, linewidth=1.5,
        markerstrokewidth=0.5, markersize=8)
    plot!(nsites, direct_bps, label=L"$h^{\phantom{(3)}}$",
        color=truedyn_col, markershape=truedyn_shape, linewidth=1.5,
        markerstrokewidth=0.5, markersize=8)

    savefig(outfname)
    return p1
end

function plot_twotrack_main(maindf, outfname)
    kSlow = maindf[1, :kslow]
    kFast = maindf[1, :kfast]

    df = filter(x -> x.kfast == kFast && x.kslow == kSlow, maindf) |> sort

    true_bps = df.true_bits ./ df.nsteps
    # Data from df is is step representation
    m1_bps = df.m0_bits ./ df.nsteps
    m2_bps = df.m1_bits ./ df.nsteps

    p1 = plot(xaxis=:log, legend=false,
        xlabel=L"\log (\gamma/k_s)", ylabel=bitrate_label,
        xlim=(0.5e-3, 110), ylim=(0.015, 0.0195),
        dpi=1200, size=(500, 300), xlabelfontsize=18, ylabelfontsize=18,
        xtickfontsize=14, ytickfontsize=14)
    plot!(p1, df.kswitch ./ kSlow, m1_bps, label=L"$\tilde{h}^{(1)}$",
        color=markov1_col, markershape=markov1_shape, markersize=5.0,
        linewidth=0.5, linealpha=0.7, markerstrokewidth=0.5)
    plot!(p1, df.kswitch ./ kSlow, m2_bps, label=L"$\tilde{h}^{(2)}$",
        color=markov2_col, markershape=markov2_shape, markersize=5.0,
        linewidth=0.5, linealpha=0.7, markerstrokewidth=0.5)
    plot!(p1, df.kswitch ./ kSlow, true_bps, label=L"$\tilde{h}^{\phantom{(2)}}$",
        color=truedyn_col, markershape=truedyn_shape, markersize=5.0,
        linewidth=0.5, linealpha=0.7, markerstrokewidth=0.5)

    savefig(p1, outfname)
    return p1
end

function plot_twotrack_ratediff(maindf, static_df, forgetful_df, outfname)
    # TODO: Replace this when we actually have a df
    sds = [s[1] / s[2] for (_, s) in static_df]
    fds = [f[1] / f[2] for (_, f) in forgetful_df]

    kSlow = maindf[1, :kslow]
    kFast = maindf[1, :kfast]

    df = filter(x -> x.kfast == kFast && x.kslow == kSlow, maindf) |> sort

    slow_row = filter(x -> x.kfast == kSlow && x.kslow == kSlow, maindf)
    slow_bps = slow_row[1, :true_bits] / slow_row[1, :nsteps]
    fast_row = filter(x -> x.kfast == kFast && x.kslow == kFast, maindf)
    fast_bps = fast_row[1, :true_bits] / fast_row[1, :nsteps]

    maxsw_row = filter(x -> x.kswitch == maximum(df.kswitch), maindf)
    maxsw_bps = maxsw_row[1, :true_bits] / maxsw_row[1, :nsteps]
    maxsw_plugin = mean(df.m1_plugin[end-10:end])

    correction = maxsw_plugin - maxsw_bps

    lowlim = (fast_bps + slow_bps) / 2

    true_bps = df.true_bits ./ df.nsteps
    m1_bps = df.m0_bits ./ df.nsteps

    plot_ylims = (0.0096, 0.0111)

    # Here, we dodge a bit, and use the max-switchrate plugin estimator in place
    # of the true markov estimator to avoid statistical issues
    delta_bps = true_bps - m1_bps .+ maxsw_plugin

    lowlim += correction

    p1 = plot(xaxis=:log, legend_position=:bottomright,
        xlabel=L"\log (\gamma / k_s)", ylabel=bitrate_label,
        xlim=(1e-4, 100), ylim=plot_ylims,
        dpi=1200, size=(600, 350), legendfontsize=13, xlabelfontsize=15,
        ylabelfontsize=15, xtickfontsize=13, ytickfontsize=13)

    plot!(p1, df.kswitch ./ kSlow, [lowlim for _ in df.kswitch], color=:black, linestyle=:dot,
        linewidth=4, label=L"\frac{h_f + h_s}{2}")

    scatter!(p1, df.kswitch ./ kSlow, delta_bps, markershape=:star, color=:magenta,
        markerstrokewidth=0.5, markersize=10, label=L"h")

    sdcolor = :firebrick1
    fgtcolor = :goldenrod2

    plot!(p1, df.kswitch ./ kSlow, [mean(sds) + correction for _ in df.kswitch], color=sdcolor, label="mean(static disorder)")
    plot!(p1, df.kswitch ./ kSlow, [mean(fds) + correction for _ in df.kswitch], color=fgtcolor, label="mean(forgetful model)")

    # Plot these off of the main canvas so that we get them in a single legend
    # for the inset
    plot!(p1, [0.0], [1.0], label=L"$\tilde{h}^{(1)}$ (inset)",
        color=markov1_col, markershape=markov1_shape, markersize=5.0,
        linewidth=0.5, linealpha=0.7, markerstrokewidth=0.5)
    plot!(p1, [0.0], [1.0], label=L"$\tilde{h}^{(2)}$ (inset)",
        color=markov2_col, markershape=markov2_shape,
        linewidth=0.5, linealpha=0.7, markerstrokewidth=0.5)
    plot!(p1, [0.0], [1.0], label=L"$\tilde{h}^{\phantom{(2)}}$ (inset)",
        color=truedyn_col, markershape=truedyn_shape,
        linewidth=0.5, linealpha=0.7, markerstrokewidth=0.5)

    sds_rezero = sds .+ correction
    fds_rezero = fds .+ correction
    fds_mean = mean(fds_rezero)

    println(fds_mean)

    p2 = plot(xlabel="Probability Density", legendfontsize=14,
        xlabelfontsize=14, ylabelfontsize=14, xtickfontsize=12, ytickfontsize=12,
        yticks=nothing)
    plot!(p2, [0.0, 0.3], [fds_mean, fds_mean], color=fgtcolor, label="Forgetul Model")
    histogram!(p2, sds_rezero, color=sdcolor, linecolor=nothing, orientation=:horizontal,
        ylim=plot_ylims, label="Static Disorder", alpha=0.7,
        bins=collect(range(minimum(sds_rezero), stop=maximum(sds_rezero), step=5e-5)),
        normalize=:probability)

    p3 = plot(legend=false, grid=false, foreground_color_subplot=:white, framestyle=:none)
    p4 = plot(legend=false, grid=false, foreground_color_subplot=:white, framestyle=:none)

    p3 = plot(p1, p3, p2, p4, link=:y, layout=grid(1, 4, widths=[0.7, 0.01, 0.28, 0.01]), size=(1000, 500), dpi=1200)

    savefig(p3, outfname)
    return p1
end

if abspath(PROGRAM_FILE) == @__FILE__
    targetdir = 1

    sfd_df = deserialize("data/bitrates/$(targetdir)/sfd.jls")
    twotrack_df = deserialize("data/bitrates/$(targetdir)/twotrack.jls")
    static_df = deserialize("data/bitrates/$(targetdir)/staticdata.jls")
    forgetful_df = deserialize("data/bitrates/$(targetdir)/forgetfuldata.jls")

    sfd_direct_df = filter(x -> x.kind == "direct", sfd_df)

    plot_sfd_df_direct(sfd_direct_df, "sfd_direct.svg")

    # Due to a change in presentation, these "main" plot is now inset
    plot_twotrack_main(twotrack_df, "tt_inset.svg")
    plot_twotrack_ratediff(twotrack_df, static_df, forgetful_df, "tt_ratediff.svg")
end