module TwoSpeed
include("../../lib/lib.jl")

using Base.Threads
using Logging

export MotorState, RateCoeffs, run_static_disorder_sim, sds_state_train

struct MotorState
    pos::Int8
end

struct RateCoeffs
    fast::Float64
    slow::Float64
end

function get_or_gen_speed(state_id, state_table)
    if haskey(state_table, state_id)
        return state_table[state_id]
    end
    state_table[state_id] = rand(Bool)
end

function next_transition(cur_transition, rate_coeffs, cur_pos, state_table)
    fast = get_or_gen_speed(cur_pos, state_table)

    # Respectively the forward, backward, and switching rate coefficients
    fastr = rate_coeffs.fast
    slowr = rate_coeffs.slow

    ws = fast ? [fastr, fastr] : [slowr, slowr]
    dt = find_t(ws)

    @assert(!isnan(dt) && !isinf(dt), "Infinite time")

    pos = sample(Int8.([-1, 1]), weights(ws))

    ms = MotorState(pos)
    time = cur_transition.time + dt

    StateTransition(ms, time)
end

function run_static_disorder_sim(maxtime, coeffs)
    init_pos = 0
    init = MotorState(init_pos)
    state = StateTransition(init, 0.0)
    state_table = Dict{Int,Bool}()   # Tracks whether this state is fast or slow
    cur_pos = init_pos                # Current (spatial) state
    transitions = Vector{StateTransition{MotorState}}()
    while state.time < maxtime
        push!(transitions, state)
        state = next_transition(state, coeffs, cur_pos, state_table)
        cur_pos += state.state.pos
    end
    (transitions, state_table, init_pos)
end

function sds_state_train(transitions, dt)
    gen_state_train(transitions, dt, x -> x.pos)
end

end