# Construct the two-track paper data for publication

using Serialization
using Base.Threads
using Logging

# const numpts = 2_000_000_000  # From paper
const numpts = 100_000_000  # For testing

#= Data needed:
    - True Dynamics
    - 1-Markov
    - 2-Markov
    - Static Disorder (single line)
    - Fast reference
    - Slow reference
=#

const dt = 0.0008

include("twotrack.jl")
include("twospeed.jl")
include("../../lib/lib.jl")

const maxt = numpts * dt
const kfast = 1.0
const kslow = 0.1

maxswrate = kfast * 10
minswrate = kslow / 100_000
switchrates = 10 .^ range(log10(minswrate), stop = log10(maxswrate), length = 128)

##### SWITCHING DATA

l = SpinLock()
data = Dict()
@threads for ksw in switchrates
    mmt = TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kfast, kslow, ksw))
    ttrain = TwoTrack.mmm_state_train(mmt, dt)
    itrain = generate_indep_trace(ttrain)
    mtrain = generate_markov_trace(ttrain)

    tbits = compressed_size_bits(ttrain)
    ibits = compressed_size_bits(itrain)
    mbits = compressed_size_bits(mtrain)

    nsteps = length(ttrain)
    ntransitions = length(mmt)
    @info "Completed computation for $(ksw) with $(ntransitions) transitions"
    lock(l) do
        data[ksw] = (tbits, ibits, mbits, nsteps, ntransitions)
    end
end

serialize("tracedata.jls", data)

#### FAST AND SLOW REFERENCE DATA

fastref = TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kfast, kfast, kfast))
fasttrain = TwoTrack.mmm_state_train(fastref, dt)
fastbits = compressed_size_bits(fasttrain)
fastdat = (fastbits, length(fasttrain), length(fastref))

@info "Finished fast reference"

slowref = TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kslow, kslow, kslow))
slowtrain = TwoTrack.mmm_state_train(slowref, dt)
slowbits = compressed_size_bits(slowtrain)
slowdat = (slowbits, length(slowtrain), length(slowref))

@info "Finished slow reference"

serialize("refdata.jls", (fastdat, slowdat))

#### STATIC DISORDER DATA

(mmst, _, _) = TwoSpeed.run_static_disorder_sim(maxt, TwoSpeed.RateCoeffs(kfast, kslow))
strain = TwoSpeed.sds_state_train(mmst, dt)
sbits = compressed_size_bits(strain)
ssteps = length(strain)
stransitions = length(mmst)

@info "Finished static data"

serialize("staticdata.jls", (sbits, ssteps, stransitions))
