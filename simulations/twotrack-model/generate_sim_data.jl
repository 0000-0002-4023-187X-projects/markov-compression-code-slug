# Construct the two-track paper data for publication

using Serialization
using Base.Threads
using Logging

const numpts = 2_000_000_000  # From paper
#const numpts = 10  # For testing

#= Data needed:
    - True Dynamics
    - 1-Markov
    - 2-Markov
    - Static Disorder (single line)
    - Fast reference
    - Slow reference
=#

const dt = 0.0008

include("twotrack.jl")
include("twospeed.jl")
include("../../lib/lib.jl")

const maxt = numpts * dt
const kfast = 1.0
const kslow = 0.1

maxswrate = kfast * 10
minswrate = kslow / 100_000
switchrates = 10 .^ range(log10(minswrate), stop=log10(maxswrate), length=128)

##### SWITCHING DATA

l = SpinLock()
data = Dict()
@threads for ksw in switchrates
    mmt = TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kfast, kslow, ksw))
    @info "Completed computation for $(ksw)"
    lock(l) do
        data[ksw] = mmt
    end
end

serialize("raw_tracedata.jls", data)

#### FAST AND SLOW REFERENCE DATA

fastref = TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kfast, kfast, kfast))
fastdat = fastref

@info "Finished fast reference"

slowref = TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kslow, kslow, kslow))
slowdat = slowref

@info "Finished slow reference"

serialize("raw_refdata.jls", (fastdat, slowdat))

#### STATIC DISORDER DATA

(mmst, _, _) = TwoSpeed.run_static_disorder_sim(maxt, TwoSpeed.RateCoeffs(kfast, kslow))

@info "Finished static data"

serialize("raw_staticdata.jls", mmst)
