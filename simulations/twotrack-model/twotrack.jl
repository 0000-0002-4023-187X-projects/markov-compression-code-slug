module TwoTrack
include("../../lib/lib.jl")

using Base.Threads
using Logging

export MotorState, RateCoeffs, run_mm, run_mm_equal_slowfast, mmm_state_train

struct MotorState
    pos::Int8
    fasttrack::Bool
end

struct RateCoeffs
    fast::Float64
    slow::Float64
    switch::Float64
end

function next_transition(cur_transition, rate_coeffs)
    fast = cur_transition.state.fasttrack

    # Respectively the forward, backward, and switching rate coefficients
    (fw, bk, sw) = if fast
        rate_coeffs.fast, rate_coeffs.fast, rate_coeffs.switch
    else
        rate_coeffs.slow, rate_coeffs.slow, rate_coeffs.switch
    end

    ws = [bk, sw, fw]
    dt = find_t(ws)

    @assert(!isnan(dt) && !isinf(dt), "Infinite time")

    x = sample([-1, 0, 1], weights(ws))
    if x == 0
        pos = 0
        fast = !fast
    else
        pos = x
    end

    ms = MotorState(pos, fast)
    time = cur_transition.time + dt

    StateTransition(ms, time)
end

function run_mm(maxtime, coeffs, fast_init = true)
    init = MotorState(0, fast_init)
    state = StateTransition(init, 0.0)
    transitions = Vector{StateTransition{MotorState}}()
    while state.time < maxtime
        push!(transitions, state)
        state = next_transition(state, coeffs)
    end
    transitions
end

"""Runs the requested time, then continues running until time spent in the slow
and fast state are within 0.1% of each other."""
function run_mm_equal_slowfast(maxtime, coeffs, fast_init = true)
    init = MotorState(0, fast_init)
    state = StateTransition(init, 0.0)
    transitions = Vector{StateTransition{MotorState}}()
    while state.time < maxtime
        push!(transitions, state)
        state = next_transition(state, coeffs)
    end

    @info "Finished base trace for $(coeffs)"

    # Compute occupancy time fractions
    (fast_time, slow_time) = compute_occupancy_fraction(transitions)

    fast_frac(ft, st, δ, is_fast) =
        if is_fast
            (ft + δ) / (ft + st + δ)
        else
            ft / (ft + st + δ)
        end
    fast_frac(ft, st) = fast_frac(ft, st, 0.0, true)
    state = transitions[end]

    CUTOFF = 0.001
    orig_time = fast_time + slow_time
    # Attempt to "manipulate probability" by continuing to simulate the system
    # past its endpoint, until the time spent in either state is within 0.1%
    while abs(fast_frac(fast_time, slow_time) - 0.5) > CUTOFF
        is_fast = transitions[end].state.fasttrack
        state = next_transition(state, coeffs)
        push!(transitions, state)
        dt = transitions[end].time - transitions[end-1].time

        # If the time ratio with or without this transition is over/under 0.5,
        # we have no choice but to add it and continue sampling. However, if it
        # crosses 0.5, we can get exactly 0.5 by truncating the timestep. This
        # is most useful when the timesteps are large, which is also when
        # we're likely to hit an OOM issue.

        cur_frac = fast_frac(fast_time, slow_time) - 0.5
        new_frac = fast_frac(fast_time, slow_time, dt, is_fast) - 0.5


        if fast_time + slow_time > 4 * orig_time
            # We could keep trying to extend this, but it's possible to hit an
            # OOM condition while trying. Instead, reject this trace and let the
            # wrapping code figure out how to deal with the failure
            @warn "Failed to equalize sim on $(coeffs)s"
            return nothing
        end

        if cur_frac * new_frac > 0
            if is_fast
                fast_time += dt
            else
                slow_time += dt
            end
            continue # Nothing we can do: have to keep sampling
        end

        # We've crossed the 0.5 marker: find the fraction of dt needed to get
        # within the target zone and overwrite the last transition with a new
        # transition which is truncated to meet the 50% criteria.
        adj_dt = abs(fast_time - slow_time)
        @assert(
            adj_dt < dt,
            "Adjusted δ is larger than permissible: sampled dt was $(dt) but adjusted is $(adj_dt)"
        )
        transitions[end] =
            StateTransition(transitions[end].state, transitions[end-1].time + adj_dt)
    end
    transitions
end

function mmm_state_train(transitions, dt)
    gen_state_train(transitions, dt, x -> x.pos)
end

"""Computes the amount of time that fast and slow states were occupied in a trace"""
function compute_occupancy_fraction(mmt)
    traninfo = []
    for i = 1:length(mmt)-1
        push!(
            traninfo,
            (mmt[i].state.fasttrack, mmt[i].state.pos, mmt[i+1].time - mmt[i].time),
        )
    end
    fast_steps = [z[3] for z in filter(x -> x[1], traninfo)]
    slow_steps = [z[3] for z in filter(x -> !x[1], traninfo)]
    fast_time = sum(fast_steps, init = 0)
    slow_time = sum(slow_steps, init = 0)

    (fast_time, slow_time)
end

end