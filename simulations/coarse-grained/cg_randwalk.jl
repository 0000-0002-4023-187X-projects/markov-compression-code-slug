includet("../../lib/lib.jl")
using Serialization

coarse_grain(states, groupsz) = floor.(states ./ groupsz) .|> Int

"""Generates a coarse-grained random walk on the integers. Because of type
limitations, expresses the result as a +1/-1 walk on the coarse-grained 
states. Because we store coarse-grained states as Int64 before coarse-graining,
it is theoretically possible for this to fail if nsteps > 2^63."""
function generate_cgwalk_steps(nsteps, groupsz)
    states = Vector{Int32}(undef, nsteps)
    curstate = 0
    for i = 1:nsteps
        states[i] = curstate
        curstate += rand((-1, 1))
    end

    cgstates = coarse_grain(states, groupsz)
    deltas = filter(x -> x != 0, (diff(cgstates) .|> Int8))

    return deltas
end

"""Generate reference traces for a walk created by generate_cgwalk_steps and compute
entropy via compression. Outputs compressed sizes of traces and the number of steps
in the trace."""
function run_comparison(nmar_trace)
    indep_trace = generate_indep_trace(nmar_trace)
    smar_trace = generate_markov_trace(nmar_trace)

    @assert(all(length.([nmar_trace, indep_trace, smar_trace]) .== length(nmar_trace)))
    @assert(is_delta_trace(nmar_trace))
    @assert(is_delta_trace(indep_trace))
    @assert(is_delta_trace(smar_trace))

    trace_len = length(nmar_trace)

    nmar_bits = compressed_size_bits(nmar_trace)
    indep_bits = compressed_size_bits(indep_trace)
    smar_bits = compressed_size_bits(smar_trace)

    return (true_bits=nmar_bits, m0_bits=indep_bits, m1_bits=smar_bits, nsteps=trace_len)
end

"""Generate all cgwalks under study"""
function generate_all_cgwalks(max_group_size, nrawsteps)
    out = Vector{Any}(undef, max_group_size)
    @threads for i = 1:max_group_size
        trace = generate_cgwalk_steps(nrawsteps, i)
        out[i] = run_comparison(trace)
    end
    return out
end