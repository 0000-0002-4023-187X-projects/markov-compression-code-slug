include("cg_randwalk.jl")
using Serialization

max_group_size = 10

# The number of raw (ungrouped) steps taken
# nrawsteps = 100_000_000   # Value used in paper
nrawsteps = 10_000_000    # Suggested value for balancing speed + results

generate_cgwalk(max_group_size, nrawsteps, "cgwalk_bps.jls")