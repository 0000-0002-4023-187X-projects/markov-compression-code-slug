Coarse Grained Random Walk
=========================

To generate data, run `julia --threads <T> --project=../.. generate_data.jl`, where `<T>` is the
number of threads to use.

To plot, run the `generate_plots.ipynb` notebook after generating the data.

Expected runtime: ~11 CPU-minutes for provided parameters.

Parameters: 
  - `nrawsteps`: the number of steps to take in the non-coarse-grained walk.
  - `max_group_size`: the largest group size to try. Will sweep over group
                          sizes from 1 to this value
                          
Memory usage: no more than 1GB/thread.
