using Base.Threads
using Serialization
using DataFrames
using Logging
includet("../single-file-diffusion/sf_diffusion.jl")

noisy_data = deserialize("../../data/rawwalks/sfd_noisy.jls")

out = DataFrame(noise_rate=Float64[],
    kind=String[], nwalkers=Int[], nsites=Int[],
    nsteps=Int[], true_bits=Int[], m0_bits=Int[], m1_bits=Int[],
    m2_bits=Int[], m3_bits=Int[], m0_plugin=Float64[], m1_plugin=Float64[],
    m2_plugin=Float64[], m3_plugin=Float64[])
noise_rates = [keys(noisy_data)...]
nw_ns = [keys(noisy_data[noise_rates[1]])...]

c = Channel() do ch
    for nr in noise_rates, (nw, ns) in nw_ns
        put!(ch, (nr, nw, ns))
    end
end

l = SpinLock()
function analyze_param((nr, nw, ns))
    trace = noisy_data[nr][(nw, ns)]

    res1 = analyze_particle(trace, identity)
    res2 = analyze_particle(trace, gen_deltas_noisy)

    res1 = (noise_rate=nr, kind="direct", nwalkers=nw, nsites=ns, res1...)
    res2 = (noise_rate=nr, kind="delta", nwalkers=nw, nsites=ns, res2...)
    @lock l push!(out, res1)
    @lock l push!(out, res2)
end

Base.Threads.foreach(analyze_param, c)

serialize("../../data/bitrates/1/sfd_noisy.jls", out)