using Base.Threads
using Serialization
using StatsBase
includet("../single-file-diffusion/sf_diffusion.jl")

function noise_sfd(trace, misread_prob, ringsize)
    newtrace = copy(trace)
    for i in eachindex(trace)
        newtrace[i] += sample([-1, 0, 1], Weights([misread_prob, 1 - 2 * misread_prob, misread_prob]))
        newtrace[i] = map_to_ring(newtrace[i], ringsize)
    end
    return newtrace
end

function postprocess_sfd(data, misread_prob)
    newdata = Dict()
    ks = [keys(data)...]
    l = SpinLock()
    @threads for k in ks
        (_, nsites) = k
        v = data[k]
        r = v[2, :]
        noisy = noise_sfd(r, misread_prob, nsites)
        @lock l newdata[k] = noisy
    end
    newdata
end

function postprocess_sfds(data, misread_probs)
    out = Dict()
    for nr in misread_probs
        out[nr] = postprocess_sfd(data, nr)
    end
    out
end

if abspath(PROGRAM_FILE) == @__FILE__
    noiserates = [1e-4, 1e-3, 1e-2, 1e-1]
    data = deserialize(ARGS[1])
    newdata = postprocess_sfds(data, noiserates)
    serialize(ARGS[2], newdata)
end