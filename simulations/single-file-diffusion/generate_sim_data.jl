include("sf_diffusion.jl")
using Base.Threads
using Serialization

NSTEPS = 100_000_000    # Value used in paper

nsteps = NSTEPS

l = SpinLock()
out = Dict()

nw_ns = [(7, ns) for ns = 8:21]
push!(nw_ns, (2, 3))
filter!(x -> x[2] > x[1], nw_ns)

@info "Running sim for parameters: $(nw_ns)"

Base.Threads.@threads for param_i = 1:length(nw_ns)
    (nw, ns) = nw_ns[param_i]
    localres = Vector{Any}(undef, nw)
    (sim, _) = run_sim(nw, ns, nsteps)
    for i = 1:nw
        trace = get_particle_trace(sim, i)
        localres[i] = trace
    end
    @lock l out[(nw, ns)] = localres
end
serialize("sfd_raw_data.jls", out)