Single File Diffusion
=====================

To generate data, run `sf_diffusion_delta.jl` and `sf_diffusion_direct.jl`.

To change data size, modify `nsteps` variable.

Then run `sfd.ipynb` to generate plots.