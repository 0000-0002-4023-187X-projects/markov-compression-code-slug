using StaticArrays
using StatsBase
using Logging
using Base.Threads
using Serialization

include("../../lib/lib.jl")

# N = number of spaces
# M = number of particles
mutable struct RingWalkerState{N,M}
    positions::MVector{M,Int8}
    time::Float64
end

# Can change if needed to accomodate more states
const STATE_TY = Int8

RingWalkerState(init_states, num_spaces) = begin
    M = length(init_states)
    RingWalkerState{num_spaces,M}(init_states, 0.0)
end

num_sites(x::RingWalkerState) = typeof(x).parameters[1]
num_particles(x::RingWalkerState) = typeof(x).parameters[2]

function map_to_ring(x, n)
    (x + n - 1) % n + 1
end

"""Remap sites into ring on 1:N"""
function map_to_ring(sim)
    n = num_sites(sim)
    map!(x -> map_to_ring(x, n), sim.positions, sim.positions)
end

"""Modifies the input by taking a single step and updating the time."""
function move_particle(state)
    n = num_sites(state)
    valid_moves = Vector{Tuple{STATE_TY,Int8}}()
    for i = 1:num_particles(state)
        cpos = state.positions[i]
        if map_to_ring(cpos - 1, n) ∉ state.positions
            push!(valid_moves, (i, -1))
        end
        if map_to_ring(cpos + 1, n) ∉ state.positions
            push!(valid_moves, (i, 1))
        end
    end
    state.time -= log(rand()) * (1 / length(valid_moves))
    (walker, delta) = sample(valid_moves)
    state.positions[walker] += delta
    map_to_ring(state)

    @assert(length(unique(state.positions)) == length(state.positions))
    nothing
end

"""Runs a ring walker simulation, recording all positions and times"""
function run_sim(nwalkers, nsites, nsteps)
    state = RingWalkerState(collect(1:nwalkers), nsites)

    trace = Matrix{STATE_TY}(undef, nwalkers, nsteps)
    timetrace = Vector{Float64}(undef, nsteps)

    for i = 1:nsteps
        trace[:, i] = state.positions
        timetrace[i] = state.time
        move_particle(state)
    end

    (trace, timetrace)
end

"""Generates a delta trace, incorporating wraparounds"""
function gen_deltas(base_trace)
    delta = diff(base_trace, dims=1) .|> Int8
    @assert(delta |> unique |> length <= 5, "Too many deltas!")
    remap(x) =
        if x > 1
            Int8(-1)
        elseif x < -1
            Int8(1)
        else
            x
        end

    map(remap, delta)
end

function gen_deltas_noisy(base_trace)
    maxsite = maximum(base_trace)
    newtrace = zeros(Int8, length(base_trace))

    for i in 1:length(base_trace)-1
        y = base_trace[i+1]
        x = base_trace[i]
        d = abs(y - x)
        if d > maxsite / 2
            d = maxsite - d
        end
        d = if abs(mod(x + d, maxsite) - y) < 1e-6
            d
        else
            -d
        end
        newtrace[i] = d
    end
    return newtrace
end

"""Gets the trace of a single particle"""
function get_particle_trace(trace, i)
    trace[i, :]
end

function analyze_particle(particle_trace, transform_func)
    ptrace = transform_func(particle_trace)
    itrace = generate_indep_trace(ptrace)
    mtrace = generate_markov_trace(ptrace)
    m2trace = generate_korder_trace(ptrace, 2)
    m3trace = generate_korder_trace(ptrace, 3)

    psize = compressed_size_bits(ptrace)
    isize = compressed_size_bits(itrace)
    msize = compressed_size_bits(mtrace)
    m2size = compressed_size_bits(m2trace)
    m3size = compressed_size_bits(m3trace)

    indep_est = estimate_indep_entropy(ptrace)
    mark_est = estimate_markov_entropy(ptrace)
    m2_est = estimate_korder_entropy(ptrace, 2)
    m3_est = estimate_korder_entropy(ptrace, 3)

    nsteps = length(particle_trace)

    (true_bits=psize, m0_bits=isize, m1_bits=msize, m2_bits=m2size,
        m3_bits=m3size, m0_plugin=indep_est, m1_plugin=mark_est, m2_plugin=m2_est,
        m3_plugin=m3_est, nsteps=nsteps)

end

function generate_data(nsteps, outfname, transform_func=identity)
    l = SpinLock()
    out = Dict()

    nw_ns = [(7, ns) for ns = 8:21]
    push!(nw_ns, (2, 3))
    filter!(x -> x[2] > x[1], nw_ns)

    @info "Running sim for parameters: $(nw_ns)"

    Base.Threads.@threads for param_i = 1:length(nw_ns)
        (nw, ns) = nw_ns[param_i]
        localres = Vector{Any}(undef, nw)
        (sim, _) = run_sim(nw, ns, nsteps)
        for i = 1:nw
            trace = get_particle_trace(sim, i)
            localres[i] = analyze_particle(trace, transform_func)
        end
        @lock l out[(nw, ns)] = localres
        @info "Finished ($(nw),$(ns))"
    end
    serialize(outfname, out)
end
