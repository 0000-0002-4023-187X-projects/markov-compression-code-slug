include("sf_diffusion.jl")

# NSTEPS = 100_000_000    # Value used in paper
NSTEPS = 10_000_000       # Still decent results, but not as close to true bps

using Base.Threads

generate_data(NSTEPS, "sfd_direct_bps.jls", identity)
generate_data(NSTEPS, "sfd_delta_bps.jls", diff)