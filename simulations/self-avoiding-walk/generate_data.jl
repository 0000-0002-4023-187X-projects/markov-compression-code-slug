include("saw_pivot.jl")

# The number of raw samples to take in the markov chain. Every 2000 steps will
# be skipped to get independent samples, resulting in NRAWSTEPS / 2000 samples
# taken of self-avoiding walks before compression.

NRAWSTEPS = 100_000_000     # Value used for paper

using Base.Threads

ns = 50:50:1000
l = SpinLock()
d = Dict()
@threads for n in ns
    sim = get_fullsim_for_length(n, NRAWSTEPS)
    @lock l d[n] = sim
end
serialize("saw_raw_data.jls", d)

# generate_data(NRAWSTEPS)
