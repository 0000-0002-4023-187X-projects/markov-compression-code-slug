Self Avoiding Walk
=================

To generate simulations, run `saw_pivot.jl`.

To modify number of steps, change the NRAWSTEPS variable.

To generate plots, run `saw.ipynb`.