using Logging
using Base.Threads
using Serialization

include("../../lib/lib.jl")

const SKIP_EVERY = 2000

m_list = zeros(2, 2, 8)
m_list[:, :, 1] = [1 0; 0 1]
m_list[:, :, 2] = [0 -1; 1 0]
m_list[:, :, 3] = [-1 0; 0 -1]
m_list[:, :, 4] = [0 1; -1 0]
m_list[:, :, 5] = [1 0; 0 -1]
m_list[:, :, 6] = [0 1; 1 0]
m_list[:, :, 7] = [-1 0; 0 1]
m_list[:, :, 8] = [0 -1; -1 0]

is_saw(walk) = length(Set(eachcol(walk))) == size(walk, 2)

# A julia adaptation of the core pivoting algorithm found at
# https://github.com/gabsens/SelfAvoidingWalk/blob/master/SAW.ipynb
function gen_pivots(walk_len, nraw_samples)
    # Do dumb initialization: one long line in the +y direction
    cur_walk = zeros(2, walk_len)
    cur_walk[2, :] = [i for i in 1:walk_len]
    nsamples = nraw_samples ÷ SKIP_EVERY
    ctr = 1
    results = zeros(Int, 2, walk_len, nsamples)
    for i in 1:nraw_samples
        pivot_index = rand(1:walk_len)
        pivot = cur_walk[:, pivot_index]
        transform = m_list[:, :, rand(1:8)]
        walk_1 = cur_walk[:, 1:pivot_index]
        walk_2 = cur_walk[:, pivot_index+1:end]

        # Transform second half of walk about pivot
        walk_2 = (transform * (walk_2 .- pivot)) .+ pivot
        walk = hcat(walk_1, walk_2)
        if is_saw(walk)
            cur_walk .= walk
        end

        if i % SKIP_EVERY == 0
            results[:, :, ctr] = cur_walk
            ctr += 1
            if ctr > nsamples
                break
            end
        end
    end
    return results
end

function snapshot_to_steps(walk)
    (_, walklen) = size(walk)
    result = Int8[]

    UP = 1
    RIGHT = 2
    DOWN = 3
    LEFT = 4

    for i in 1:walklen-1
        x = walk[1, i]
        y = walk[2, i]
        xn = walk[1, i+1]
        yn = walk[2, i+1]

        if xn == x + 1
            push!(result, RIGHT)
        elseif xn == x - 1
            push!(result, LEFT)
        elseif yn == y + 1
            push!(result, UP)
        elseif yn == y - 1
            push!(result, DOWN)
        else
            error("Invalid step in self-avoiding walk frame")
        end
    end
    return result
end

function samples_to_steps(walks)
    (_, walklen, nsteps) = size(walks)
    results = zeros(Int8, walklen - 1, nsteps)
    for i in 1:nsteps
        results[:, i] = snapshot_to_steps(walks[:, :, i])
    end
    results
end

function get_fullsim_for_length(walk_len, NRAWSTEPS)
    traces = gen_pivots(walk_len, NRAWSTEPS)

    # Calculate first index after burn-in period of chain
    skipfirst = ceil(Int, 10 * walk_len^1.19)
    # For short chains, cannot afford the burnin. Should never happen at real
    # simulation sizes but is acceptable for short simulation
    if skipfirst > size(traces, 2) ÷ 5
        skipfirst = 0
    end
    steps = samples_to_steps(traces)
    steps = steps[:, skipfirst+1:end]

    return steps
end

"""Generates a random walk of length n"""
function gen_rand4_walk(n)
    steps = UInt8[1, 2, 3, 4]
    walk = Vector{UInt8}(undef, n)
    for i = 1:n
        walk[i] = sample(steps)
    end
    walk
end

"""Generates a random walk of length n which does not double back on itself"""
function gen_rand3_walk(n)
    # List of valid steps that don't double back
    steps = [
        UInt8[1 2 4],   # 3 (down) cannot come after 1 (up)
        UInt8[1 2 3],   # 4 (left) cannot come after 2 (right)
        UInt8[2 3 4],
        UInt8[1 3 4],
    ]
    walk = Vector{UInt8}(undef, n)
    lastwalk = rand(1:4)
    for i = 1:n
        walk[i] = lastwalk
        lastwalk = rand(steps[lastwalk])
    end
    walk
end

function compute_markov_probs(trace::AbstractVector{T}) where {T<:Number}
    P = zeros(4, 4)
    for i = 1:length(trace)-1
        P[trace[i], trace[i+1]] += 1
    end
    P
end

function compute_markov_probs(trace::AbstractMatrix{T}) where {T<:Number}
    Ps = Array{Int,3}(undef, 4, 4, size(trace, 2))
    @views for (j, t) in enumerate(eachcol(trace))
        Ps[:, :, j] = compute_markov_probs(t)
    end
    mean(Ps, dims=3)
end

function gen_markov_walk(length, Ps)
    out = Vector{Int8}(undef, length)
    out[1] = rand(1:4)
    for i = 2:length
        weights = Ps[out[i-1]]
        out[i] = sample(1:4, weights)
    end
    out
end

"""Generate Markov-step walks given an input walk"""
function gen_markov_walks(saw)
    P = compute_markov_probs(saw)
    Ps = [ProbabilityWeights(P[i, :]) for i = 1:4]
    out = zeros(Int8, size(saw))
    (nstep, nwalk) = size(saw)
    for j = 1:nwalk
        out[:, j] = gen_markov_walk(nstep, Ps)
    end
    out
end

"""Generate m walks of n steps each"""
function gen_rand4_walks(n, m)
    walks = Matrix{Int8}(undef, n, m)
    for i = 1:m
        walks[:, i] = gen_rand4_walk(n)
    end
    walks
end

function gen_rand3_walks(n, m)
    walks = Matrix{Int8}(undef, n, m)
    for i = 1:m
        walks[:, i] = gen_rand3_walk(n)
    end
    walks
end

function process_walk(sim, n)
    nwalk = size(sim, 2)
    r4walks = gen_rand4_walks(n, nwalk)
    r3walks = gen_rand3_walks(n, nwalk)
    marwalks = gen_markov_walks(sim)

    true_bits = compressed_size_bits(vec(sim))
    r4_bits = compressed_size_bits(r4walks)
    r3_bits = compressed_size_bits(r3walks)
    mar_bits = compressed_size_bits(marwalks)

    probs = counts(vec(sim))
    probs = probs ./ sum(probs)
    formula_entropy = entropy(probs, 2)

    numsteps = length(sim)

    return (true_bits=true_bits, r4_bits=r4_bits, r3_bits=r3_bits, m1_bits=mar_bits,
        nsteps=numsteps, m0_plugin=formula_entropy)
end

function generate_data(nrawsteps)
    ns = 50:50:1000
    l = SpinLock()
    d = Dict()
    @threads for n in ns
        sim = get_fullsim_for_length(n, nrawsteps)
        results = process_walk(sim, n)
        @info "Finished length-n walks with total size $(size(sim))"
        @lock l d[n] = results
    end

    serialize("saw_bps.jls", d)
end
