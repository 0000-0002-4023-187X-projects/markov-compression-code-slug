include("../compression.jl")
include("../markov_sys.jl")
include("../transitions.jl")

using Test
using IterTools

struct TestState
    pos::Int
    sw::Bool
end

basic = [
    StateTransition(TestState(0, true), 0.0)
    StateTransition(TestState(1, true), 1.0)
    StateTransition(TestState(-1, true), 2.0)
    StateTransition(TestState(1, true), 3.0)
]

fast_transitions = [
    StateTransition(TestState(0, true), 0.0)
    StateTransition(TestState(1, true), 0.03)
    StateTransition(TestState(1, true), 0.05)
    StateTransition(TestState(1, true), 0.6)
    StateTransition(TestState(0, true), 1.2)
]

trackswitch = [
    StateTransition(TestState(0, true), 0.0)
    StateTransition(TestState(1, true), 0.03)
    StateTransition(TestState(0, false), 0.05)
    StateTransition(TestState(1, false), 0.6)
    StateTransition(TestState(0, false), 1.2)
]

get_pos(teststate) = teststate.pos

"""Checks behavior when transitions fall exactly on sampling points"""
function test_train_gen_exact_time()
    z = gen_state_train(basic, 0.5, get_pos)
    ref = Int8.([0, 0, 1, 0, -1, 0, 1])
    z == ref
end

"""Checks behavior when accumulated transitions happen"""
function test_train_interval_too_large()
    z = gen_state_train(fast_transitions, 0.5, get_pos)
    ref = Int8.([2, 1, 0, 0])
    z == ref
end

"""Checks behavior with track switching"""
function test_train_switching()
    z = gen_state_train(trackswitch, 0.5, get_pos)
    ref = Int8.([1, 1, 0, 0])
    z == ref
end

"""Check that the strict generation function throws when appropriate"""
function test_train_interval_too_large_exact()
    gen_state_train(fast_transitions, 0.5, get_pos, true)
end

@test test_train_gen_exact_time()
@test test_train_interval_too_large()
@test test_train_switching()
@test_throws TimeStepTooLargeException test_train_interval_too_large_exact()

test_trace_exact = [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]
test_trace_messy = [1, 2, 3, 4, 3, 1, 2, 4, 1, 2, 3, 4, 2, 1, 1, 1, 1, 3, 4]
test_trace_longexact = vcat(collect(IterTools.ncycle([1, 2, 3], 10_000)), rand(1:3, 10_000))

function check_hist_exact()
    d = estimate_indep_probability(test_trace_exact)
    all((d[1] ≈ 1 / 3, d[2] ≈ 1 / 3, d[3] ≈ 1 / 3))
end

function check_hist_messy()
    d = estimate_indep_probability(test_trace_messy)
    all((d[1] ≈ 7 / 19, d[2] ≈ 4 / 19, d[3] ≈ 4 / 19, d[4] ≈ 4 / 19))
end

function check_markov_exact()
    d = estimate_markov_probability(test_trace_exact)
    all((d[1][2] == 1.0, d[2][3] == 1.0, d[3][1] == 1.0))
end

function check_markov_messy()
    d = estimate_markov_probability(test_trace_messy)
    all((
        d[1][1] ≈ 3 / 7,
        d[1][2] ≈ 3 / 7,
        d[1][3] ≈ 1 / 7,
        d[2][1] ≈ 1 / 4,
        d[2][3] ≈ 2 / 4,
        d[2][4] ≈ 1 / 4,
        d[3][1] ≈ 1 / 4,
        d[3][4] ≈ 3 / 4,
        d[4][1] ≈ 1 / 3,
        d[4][2] ≈ 1 / 3,
        d[4][3] ≈ 1 / 3,
    ))
end

function indep_trace_obeys_freqs()
    tr = generate_indep_trace(test_trace_longexact)
    prs = estimate_indep_probability(tr) |> values |> collect
    sum(abs.(prs - [1 / 3, 1 / 3, 1 / 3])) < 0.05
end

function markov_trace_obeys_freqs()
    tr = generate_markov_trace(test_trace_longexact)
    prs = estimate_markov_probability(test_trace_longexact)
    gen_prs = estimate_markov_probability(tr)

    fail = false
    for (i, sub) in prs
        for (j, prob) in sub
            localfail = abs(gen_prs[i][j] - prob) > 0.01
            if localfail
                println("For $(i) -> $(j) transition")
                println("  Original prob is $(prs[i][j])")
                println("  Post-sample prob is $(gen_prs[i][j])")
            end
            fail |= localfail
        end
    end
    !fail
end

@test check_hist_exact()
@test check_hist_messy()
@test check_markov_exact()
@test check_markov_messy()
@test indep_trace_obeys_freqs()
@test markov_trace_obeys_freqs()