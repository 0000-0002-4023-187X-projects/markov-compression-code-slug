using Serialization
using Profile
using StatProfilerHTML
include("../transitions.jl")

struct MotorState
    pos::Int8
    fasttrack::Bool
end
struct RateCoeffs
    fast::Float64
    slow::Float64
    switch::Float64
end

#coeffs = RateCoeffs(0.01, 0.01, 0.5)
#max_t = 1_000_000
# mmt = run_mm(max_t, coeffs)

mmt = deserialize("example_mmt.jls")
train = gen_state_train(mmt, 10.0, x -> x.pos) # Compilation run

Profile.clear_malloc_data()
@profilehtml gen_state_train(mmt, 0.01, x -> x.pos)