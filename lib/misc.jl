using StatsBase
function dedupe_consecutive(trace)
    [x[1] for x in rle(trace)]
end

function is_delta_trace(x)
    (lo, hi) = extrema(x)
    (lo == -1 || lo == 0) && (hi == 0 || hi == 1)
end
