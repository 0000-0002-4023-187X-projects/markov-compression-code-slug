using Colors

default_palette = theme_palette(:auto)

markov1_col = default_palette[1]
markov2_col = default_palette[2]
truedyn_col = default_palette[3]
other1_col = default_palette[4]
other2_col = default_palette[5]

markov1_shape = :circle
markov2_shape = :utriangle
truedyn_shape = :square
other1_shape = :cross
other2_shape = :diamond

bitrate_label = "Entropy Rate (bits/step)"