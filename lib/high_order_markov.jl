using IterTools
using Logging
using DataStructures
using StatsBase
# Histogram estimators for higher-order markov

# A table lookup for elements of type T, T^N * T -> Count
# Interpretation: let V be a value of type T. Tf (V1, V2, ..., Vn) exists in the
# CountTable, then the value is a dict Vk -> Count, which tells us how many times
# the sequence (V1, V2, ..., Vn, Vk) is observed in the input.
struct CountTable{T,N}
    probs::Dict{NTuple{N,T},Dict{T,Int}}
end

"""Returns number of occurrences for `probs[k1][k2]`, i.e. the number of times
(k1 * k2) is observed in the input"""
function Base.getindex(d::CountTable{T,N}, k1::NTuple{N,T}, k2::T)::Int where {T,N}
    if !haskey(d.probs, k1)
        return 0
    elseif !haskey(d.probs[k1], k2)
        return 0
    else
        return d.probs[k1][k2]
    end
end

"""Returns the dictionary for `k`, i.e.  a frequency table of symbols following
the sequence `k` in the input"""
function Base.getindex(
    d::CountTable{T,N},
    k::NTuple{N,T},
)::Union{Dict{T,Int},Nothing} where {T,N}
    if haskey(d.probs, k)
        return d.probs[k]
    else
        return nothing
    end
end

"""Returns an aggregated count vector over all sequences in the table"""
function Base.values(d::CountTable{T,N}) where {T,N}
    out = Int[]
    for (_, v1) in d.probs
        for (_, v2) in v1
            push!(out, v2)
        end
    end
    return out
end

function Base.keys(d::CountTable{T,N}) where {T,N}
    keys(d.probs)
end

function increment_count(
    probs::Dict{NTuple{N,T},Dict{T,Int}},
    prefix::NTuple{N,T},
    sym::T,
) where {N,T}
    if haskey(probs, prefix)
        if haskey(probs[prefix], sym)
            probs[prefix][sym] += 1
        else
            probs[prefix][sym] = 1
        end
    else
        probs[prefix] = Dict{T,Int}()
        probs[prefix][sym] = 1
    end
end

"""Computes k-order prefix counts on trace: for k=1 this is a pretentious markov
model, for k >= 2 this computes k-order prefix statistics"""
function compute_korder_counts(trace, k)
    if length(trace) < 2^k
        @warn "For $(k)-length markov, want at least $(2^k) samples, but only have $(length(trace))"
    end
    if k > 5
        @warn "Need >= k*2^k bytes for a k-order markov table. You have chosen k=$(k). Are you sure about this?"
        @warn "If this is an error, better Ctrl+C out quickish! I will delay for a few seconds."
        sleep(2)
    end

    ty = eltype(trace)

    counts = Dict{NTuple{k,ty},Dict{ty,Int}}()
    for i = 1:length(trace)-k
        prefix = (trace[i:i+k-1]...,)
        next = trace[i+k]
        increment_count(counts, prefix, next)
    end

    return CountTable(counts)
end

ent2(x::Number) = x == 0 ? 0 : -x * log2(x)

function estimate_korder_entropy(trace, k)
    k_counts = compute_korder_counts(trace, k)

    #=
      Let ℵ be the set of all possible letters in the trace and V ⊆ ℵ^k be the
      set of all observed length-k substrings in the trace. The k-th order
      entropy estimate is defined by:

        S_k(trace) = ∑_{v ∈ V} p(v) ( ∑_{x ∈ ℵ} p(x|v) log2 p(x|v) )

      So we need to compute the following sets of quantities for each prefix
      v ∈ V:

        - probability of observing v
        - for each x that follows v, p(x | v)
    =#

    total_counts = sum(values(k_counts))
    entropy = 0.0

    for v in keys(k_counts)
        count_v = sum(values(k_counts[v]))
        prob_v = count_v / total_counts
        for x in keys(k_counts[v])
            count_x = k_counts[v][x]
            p_x_given_v = count_x / count_v
            # Ent2 p_x_given_v is mostly-fine numerically, (since it conditions
            # on v), but prob_v might be numerically problematic.
            entropy += prob_v * ent2(p_x_given_v)
        end
    end
    return entropy
end

function generate_korder_trace(trace, k)
    tracelen = length(trace)
    tracects = compute_korder_counts(trace, k)
    out = trace[1:k]
    while length(out) != tracelen
        b = length(out) - k + 1
        e = length(out)
        prefix = Tuple(out[b:e])
        next = tracects[prefix]

        if next === nothing
            @error """I tried to find probabilities for the symbol following $(prefix)
but that sequence has never been seen before!"""
            @assert(false, "Never-before-seen sequence!")
        end

        next_vals = collect(keys(next))
        next_probs = collect(values(next))
        step = sample(next_vals, FrequencyWeights(next_probs))
        push!(out, step)
    end
    return out
end

