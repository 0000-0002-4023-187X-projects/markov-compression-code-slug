using DataStructures
using StatsBase
using Serialization

"""Numerically estimate the probability of each symbol occurring"""
function estimate_indep_probability(trace)
    counts = DefaultDict{eltype(trace),Int}(0)
    for x in trace
        counts[x] += 1
    end

    totalcount = sum(values(counts))
    Dict(i => c / totalcount for (i, c) in counts)
end

function estimate_markov_probability(trace)
    ty = eltype(trace)
    syms = unique(trace)
    trans = Dict()
    for i = 1:length(trace)-1
        cur = trace[i]
        next = trace[i+1]
        if !haskey(trans, cur)
            trans[cur] = DefaultDict{eltype(trace),Int}(0)
        end
        trans[cur][next] += 1
    end

    # Fill in zero values on the transition Matrix (can be a problem e.g. if the
    # last element of the trace is unique)
    for i in syms
        if !haskey(trans, i)
            trans[i] = DefaultDict{eltype(trace),Int}(0)
        end
    end

    counts = Dict(i => sum(values(ct_dict)) for (i, ct_dict) in trans)


    # Build the probability lookup table, including explicit zeros for easier
    # use of the result in other functions
    out = Dict{ty,Dict{ty,Float64}}()
    for cur in syms
        if !haskey(out, cur)
            out[cur] = Dict{ty,Float64}()
        end
        ## Could iterate over subdict, but would be hard to include zeros
        for next in syms
            out[cur][next] = trans[cur][next] / counts[cur]
        end
    end

    ## Later users assume the dictionary is complete. Fill in zeroed values.

    for cur in syms, next in syms
        if !haskey(out[cur], next)
            out[cur][next] = 0
        end
    end
    out
end

function generate_indep_trace(trace)
    prob = estimate_indep_probability(trace)
    syms = unique(trace)
    probs = [prob[s] for s in syms]
    sample(syms, ProbabilityWeights(probs), length(trace))
end

function generate_markov_trace(trace)
    prob = estimate_markov_probability(trace)
    syms = unique(trace)
    probs = Dict()
    for i in syms
        probs[i] = ProbabilityWeights([prob[i][j] for j in syms])
    end

    out = Vector{eltype(trace)}(undef, length(trace))
    out[begin] = trace[begin]
    for i = 2:length(trace)
        cur = out[i-1]
        next = sample(syms, probs[cur])
        out[i] = next
    end
    out
end

entropy2(x) = entropy(x) / log(2)

function estimate_indep_entropy(trace)
    ps = estimate_indep_probability(trace)
    entropy2(values(ps))
end

function estimate_markov_entropy(trace)
    pis = estimate_indep_probability(trace)
    pms = estimate_markov_probability(trace)

    # Conditional sum formula
    S = 0.0
    for x in keys(pis)
        S_y_given_x = entropy2(values(pms[x]))
        S += pis[x] * S_y_given_x
    end
    S
end
