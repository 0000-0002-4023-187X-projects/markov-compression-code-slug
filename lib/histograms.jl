using DataStructures
using StatsBase
using Serialization

"""Numerically estimate the probability of each symbol occurring"""
function estimate_indep_probability(trace)
    counts = DefaultDict{eltype(trace),Int}(0)
    for x in trace
        counts[x] += 1
    end

    totalcount = sum(values(counts))
    Dict(i => c / totalcount for (i, c) in counts)
end
