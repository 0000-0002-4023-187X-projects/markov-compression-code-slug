using Logging
using IterTools

#=
 This entire file is meant to operate with delta encodings. Direct encodings will
 need a different library--making the core logic generic over different kinds
 of state encodings leads to a clusterfuck because I'm not good enough with
 Julia's type system to encode such mappings in a safe and performant manner.
=#

struct StateTransition{T}
    state::T
    time::Float64
end

struct TimeStepTooLargeException <: Exception end

"""Find the time at which a reaction occurs given a single rate coefficient"""
find_t(k::Real) = -1 / k * log(rand())

"""Find the time at which a reaction occurs given all rate coeffiecients out of
the current state"""
find_t(v::AbstractVector{T}) where {T<:Real} = find_t(sum(v))

time_window(transitions) = (transitions[begin].time, transitions[end].time)
num_samples(transitions, dt) = begin
    (t_begin, t_end) = time_window(transitions)
    Int(ceil((t_end - t_begin) / dt)) + 1
end

# NOTE: Will always generate one more step than is technically needed (e.g. for
# transitions on (0.0,3.0) at interval 0.5, generates 7 samples, with the last
# sample corresponding to the transition at exactly 3.0). If the final transition
# is not occupied, it will always be zero.
"""Generate a state train by generating groups of transitions that occur between
each state train, then applying groupfun to each group."""
function gen_state_train(transitions, dt, statefun, strict=false)
    if !issorted(transitions, by=x -> x.time)
        sort!(transitions, by=x -> x.time)
    end

    (t_begin, t_end) = time_window(transitions)
    time_to_index(t) = Int(floor((t - t_begin) / dt)) + 1
    numel = num_samples(transitions, dt)
    samples = zeros(Int8, numel)

    if strict
        inds = map(x -> time_to_index(x.time), transitions)
        if length(unique(inds)) != length(inds)
            throw(TimeStepTooLargeException())
        end
    end

    for i = 1:length(transitions)
        out_ind = time_to_index(transitions[i].time)
        samples[out_ind] += statefun(transitions[i].state)
    end
    samples
end
