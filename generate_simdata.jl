# This file generates all the raw simulation data for this project, writing it
# to various serialized Julia files.
using Base.Threads
using Logging
l = SpinLock()

include("simulations/coarse-grained/cg_randwalk.jl")
include("simulations/self-avoiding-walk/saw_pivot.jl")
include("simulations/single-file-diffusion/sf_diffusion.jl")
include("simulations/twotrack-model/twospeed.jl")
include("simulations/twotrack-model/twotrack.jl")
include("simulations/noisy_sfd/generate_noisy_sfd.jl")

if !isdir("data/rawwalks")
    mkpath("data/rawwalks")
end
if !isdir("data/bitrates")
    mkpath("data/bitrates")
end

# Paper values. These can generally be 10x-100x lower without ill effects on the
# relative entropy rates of markov reference processes, though the absolute
# bits per symbol might suffer.
const nrawsteps_cgwalk = 100_000_000
const nrawsteps_saw = 100_000_000
const nrawsteps_sfd = 100_000_000
const numpts_twotrack = 2_000_000_000

@info "Dependency loading/precompilation complete. Starting simulation."

# Coarse Grained Walk data
const max_group_size = 10
out = Vector{Any}(undef, max_group_size)
t = @elapsed @threads for i = 1:max_group_size
    trace = generate_cgwalk_steps(nrawsteps_cgwalk, i)
    @lock l out[i] = trace
end
@info "Completed coarse-grained generation in $(t) seconds"
serialize("data/rawwalks/cgwalk.jls", out)

# Self Avoiding Walk data
out = Dict()
walk_lengths = 50:50:1000
t = @elapsed @threads for walk_length in walk_lengths
    sim = get_fullsim_for_length(walk_length, nrawsteps_saw)
    @lock l out[walk_length] = sim
end
@info "Completed SAW generation in $(t) seconts"
serialize("data/rawwalks/saw.jls", out)

# Single File Diffusion data
out = Dict()
nw_ns = [(7, ns) for ns = 8:21]
push!(nw_ns, (2, 3))
t = @elapsed @threads for param_i = 1:length(nw_ns)
    (nw, ns) = nw_ns[param_i]
    (sim, _) = run_sim(nw, ns, nrawsteps_sfd)
    @lock l out[(nw, ns)] = sim
end
@info "Completed SFD generation in $(t) seconds"
serialize("data/rawwalks/sfd.jls", out)

# Noise the SFD to create noisy stuff
# TODO: Write this

# Two Track Diffusion Data
const dt = 0.0008
const maxt = numpts_twotrack * dt
const kfast = 1.0
const kslow = 0.1

maxswrate = kfast * 10
minswrate = kslow / 100_000
switchrates = 10 .^ range(log10(minswrate), stop=log10(maxswrate), length=128)

# SWITCHING DATA
out = Dict()
t = @elapsed @threads for ksw in switchrates
    mmt = TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kfast, kslow, ksw))
    @lock l out[ksw] = mmt
end
@info "Finished twotrack switchrates in $(t) seconds"
serialize("data/rawwalks/twotrack_variable.jls", out)

#### FAST AND SLOW REFERENCE DATA

fastref_task = @spawn TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kfast, kfast, kfast))
slowref_task = @spawn TwoTrack.run_mm(maxt, TwoTrack.RateCoeffs(kslow, kslow, kslow))
sdisorder_task = @spawn TwoSpeed.run_static_disorder_sim(maxt, TwoSpeed.RateCoeffs(kfast, kslow))

fastref = fetch(fastref_task)
slowref = fetch(slowref_task)
(mmst, _, _) = fetch(sdisorder_task)
@info "Finished twotrack reference data"
serialize("data/rawwalks/twotrack_ref.jls", (fastref, slowref, mmst))
