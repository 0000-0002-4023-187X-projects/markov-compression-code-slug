using Serialization
using StatsBase
using Statistics
using DataFrames
using LaTeXStrings
using StatsPlots
includet("lib/commondefs.jl")
pyplot()

function plot_cgwalk_df(df, outfname)
    groupedwalk_S(n) = -inv(n + 1) * log2(inv(n + 1)) - n * inv(n + 1) * log2(n * inv(n + 1))

    true_bps = df.true_bits ./ df.nsteps
    m0_bps = df.m0_bits ./ df.nsteps
    m1_bps = df.m1_bits ./ df.nsteps

    offset = mean(m0_bps .- 1.0)
    true_bps_adj = true_bps .- offset
    m0_bps_adj = m0_bps .- offset
    m1_bps_adj = m1_bps .- offset

    p2 = plot(legend=:bottomleft, #title="CG Rand Walk, Adjusted by Markov Offset", 
        ylabel=bitrate_label, xlabel=L"$n$", dpi=1200, legendfontsize=12, xlabelfontsize=14,
        ylabelfontsize=14, xtickfontsize=12, ytickfontsize=12, size=(600, 350))
    scatter!(df.groupsz, m0_bps_adj, label=L"$h^{(1)}$",
        markershape=markov1_shape, color=markov1_col, markerstrokewidth=0.5)
    scatter!(df.groupsz, true_bps_adj, label=L"$h^{\phantom{(2)}}$",
        markershape=truedyn_shape, color=truedyn_col, markerstrokewidth=0.5)
    scatter!(df.groupsz, m1_bps_adj, label=L"$h^{(2)}$",
        markershape=markov2_shape, color=markov2_col, markerstrokewidth=0.5)
    plot!(df.groupsz, [1.0 for _ in df.groupsz], label=L"$h^{(1)}$ Theory",
        color=markov1_col)
    plot!(df.groupsz, groupedwalk_S.(df.groupsz), label=L"$h^{(2)}$ Theory",
        color=markov2_col)

    savefig(outfname)
    return p2
end

function plot_saw_df(df, outfname)
    saw_bps = df.true_bits ./ df.nsteps
    ran4_bps = df.r4_bits ./ df.nsteps
    ran3_bps = df.r3_bits ./ df.nsteps
    mar_bps = df.m1_bits ./ df.nsteps
    mar_plugin = df.m0_plugin   # Whoopsies on the naming here--should be m1 plugin?

    correction = mar_bps - mar_plugin
    saw_bps .-= correction
    ran4_bps .-= correction
    ran3_bps .-= correction
    mar_bps .-= correction

    p1 = plot(xlabel="Walk Length", ylabel=bitrate_label, ylim=(1.3, 2.6),
        dpi=1200, size=(600, 450))
    scatter!(df.walklen, ran4_bps, label=L"$h^{(1)}$",
        color=markov1_col, markershape=markov1_shape, markerstrokewidth=0.5)
    scatter!(df.walklen, mar_bps, label=L"$h^{(2)}$",
        color=markov2_col, markershape=markov2_shape, markerstrokewidth=0.5)
    scatter!(df.walklen, saw_bps, label=L"$h^{\phantom{(2)}}$",
        color=truedyn_col, markershape=truedyn_shape, markerstrokewidth=0.5)
    plot!(df.walklen, [2.0 for _ in df.walklen], label=L"$h^{(1)}$ [Eq. 2]", color=markov1_col)
    plot!(df.walklen, mar_plugin, label=L"$h^{(2)}$ [Eq. 3]", color=markov2_col)
    plot!(df.walklen, [log2(2.6381585) for _ in df.walklen], label="Self Avoiding (Asymptotic)",
        linestyle=:dash, color=truedyn_col)

    savefig(outfname)
    return p1
end

function plot_sfd_df_direct(df, correct, outfname)
    df = filter(x -> x.nwalkers == 7, df) |> sort
    ylims = (0.45, 0.96)    # May need to change this depending on sim size

    # DF currently contains one row per particleid + nsites, need to aggregate
    # data by taking mean over particle ids

    gdf = groupby(df, :nsites)

    # Do for remaining columns
    df = combine(gdf, :true_bits => mean, :m0_bits => mean, :m1_bits => mean,
        :m2_bits => mean, :m3_bits => mean, :m0_plugin => mean, :m1_plugin => mean,
        :m2_plugin => mean, :m3_plugin => mean, :nsites => first, :kind => first,
        :nsteps => first, renamecols=false)

    nsites = df.nsites
    direct_bps = df.true_bits ./ df.nsteps
    markov_bps = df.m1_bits ./ df.nsteps
    m2_bps = df.m2_bits ./ df.nsteps
    m3_bps = df.m3_bits ./ df.nsteps

    if correct
        correction = markov_bps .- df.m1_plugin
        direct_bps = direct_bps .- correction
        markov_bps = markov_bps .- correction
        m2_bps = m2_bps .- correction
        m3_bps = m3_bps .- correction
    end

    orderlabel(i) =
        if correct
            L"$h^{%$i}$"
        else
            L"$\tilde{h}^{%$i}$"
        end


    p1 = plot(dpi=1200, size=(600, 300), legend=:bottomright,
        xlabel=L"$R$", ylabel=bitrate_label,
        xticks=8:2:maximum(nsites), legendfontsize=16, xtickfontsize=14, ytickfontsize=14, titlefontsize=20)
    plot!(nsites, markov_bps, label=orderlabel("(1)"),
        color=markov1_col, markershape=markov1_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, m2_bps, label=orderlabel("(2)"),
        color=markov2_col, markershape=markov2_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, m3_bps, label=orderlabel("(3)"),
        color=other1_col, markershape=other1_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, direct_bps, label=orderlabel("\\phantom{3}"),
        color=truedyn_col, markershape=truedyn_shape, linewidth=0.5,
        markerstrokewidth=0.5)

    savefig(outfname)
    return p1
end

function plot_sfd_df_delta(df, correct, outfname)
    df = filter(x -> x.nwalkers == 7, df) |> sort
    ylims = (0.45, 0.96)    # May need to change this depending on sim size

    # DF currently contains one row per particleid + nsites, need to aggregate
    # data by taking mean over particle ids

    gdf = groupby(df, :nsites)

    # Do for remaining columns
    df = combine(gdf, :true_bits => mean, :m0_bits => mean, :m1_bits => mean,
        :m2_bits => mean, :m3_bits => mean, :m0_plugin => mean, :m1_plugin => mean,
        :m2_plugin => mean, :m3_plugin => mean, :nsites => first, :kind => first,
        :nsteps => first, renamecols=false)

    nsites = df.nsites
    direct_bps = df.true_bits ./ df.nsteps

    # Offset by 1 because the df mN refers to markov-order on steps
    m1_bps = df.m0_bits ./ df.nsteps
    m2_bps = df.m1_bits ./ df.nsteps
    m3_bps = df.m2_bits ./ df.nsteps

    if correct
        correction = (df.m1_bits ./ df.nsteps) .- df.m1_plugin
        direct_bps = direct_bps .- correction
        m1_bps = m1_bps .- correction
        m2_bps = m2_bps .- correction
        m3_bps = m3_bps .- correction
    end

    orderlabel(i) =
        if correct
            L"$h^{%$i}$"
        else
            L"$\tilde{h}^{%$i}$"
        end


    p1 = plot(dpi=1200, size=(600, 300), legend=:bottomright,
        xlabel=L"$R$", ylabel=bitrate_label,
        xticks=8:2:maximum(nsites), legendfontsize=16, xtickfontsize=14, ytickfontsize=14, titlefontsize=20)
    plot!(nsites, m1_bps, label=orderlabel("(1)"),
        color=markov1_col, markershape=markov1_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, m2_bps, label=orderlabel("(2)"),
        color=markov2_col, markershape=markov2_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, m3_bps, label=orderlabel("(3)"),
        color=other1_col, markershape=other1_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, direct_bps, label=orderlabel("\\phantom{3}"),
        color=truedyn_col, markershape=truedyn_shape, linewidth=0.5,
        markerstrokewidth=0.5)

    savefig(outfname)
    return p1
end

function plot_sfd_dd_encoding_quad(direct_df, delta_df, outfname)
    p1 = plot_sfd_df_direct(direct_df, false, "sfd_temp.png")
    title!(p1, "Site-Encoded, Uncorrected")
    plot!(p1, ylims=(0.55, 1.0))
    p2 = plot_sfd_df_delta(delta_df, false, "sfd_temp.png")
    title!(p2, "Step-Encoded, Uncorrected")
    plot!(p2, ylims=(0.55, 1.0))
    p3 = plot_sfd_df_direct(direct_df, true, "sfd_temp.png")
    title!(p3, "Site-Encoded, With Correction")
    plot!(p3, ylims=(0.43, 0.76))
    p4 = plot_sfd_df_delta(delta_df, true, "sfd_temp.png")
    title!(p4, "Step-Encoded, With Correction")
    plot!(p4, ylims=(0.43, 0.76))

    pmain = plot(p1, p2, p3, p4, layout=(2, 2), size=(1200, 600), dpi=1200,
        legendfontsize=16, xtickfontsize=14, ytickfontsize=14, titlefontsize=20,
        xlabelfontsize=18, ylabelfontsize=18, markersize=7)
    savefig(outfname)
end

"""Special in the plotting functions: reads data directly"""
function plot_sfd_data_gzip_quad(outfname)
    df_gz_1 = filter(x -> x.kind == "delta", deserialize("data/gzip_bitrates/1/sfd.jls"))
    df_gz_100 = filter(x -> x.kind == "delta", deserialize("data/gzip_bitrates/100/sfd.jls"))
    df_xz_1 = filter(x -> x.kind == "delta", deserialize("data/bitrates/1/sfd.jls"))
    df_xz_100 = filter(x -> x.kind == "delta", deserialize("data/bitrates/100/sfd.jls"))

    p1 = plot_sfd_df_delta(df_gz_1, false, "sfd_temp.png")
    title!(p1, L"GZIP, $10^8$ Steps")
    p2 = plot_sfd_df_delta(df_xz_1, false, "sfd_temp.png")
    title!(p2, L"LZMA, $10^8$ Steps")
    p3 = plot_sfd_df_delta(df_gz_100, false, "sfd_temp.png")
    title!(p3, L"GZIP, $10^6$ Steps")
    p4 = plot_sfd_df_delta(df_xz_100, false, "sfd_temp.png")
    title!(p4, L"LZMA, $10^6$ Steps")
    pmain = plot(p1, p2, p3, p4, layout=(2, 2), size=(1200, 600), dpi=1200,
        legendfontsize=16, xtickfontsize=14, ytickfontsize=14, titlefontsize=20,
        xlabelfontsize=18, ylabelfontsize=18, markersize=7)
    savefig(outfname)
end

function plot_sfd_data_gzip_trirow(outfname)
    df_gz_100 = filter(x -> x.kind == "delta" && x.particle_id == 1 && x.nwalkers == 7, deserialize("data/gzip_bitrates/100/sfd.jls")) |> sort
    df_xz_1 = filter(x -> x.kind == "delta" && x.particle_id == 1 && x.nwalkers == 7, deserialize("data/bitrates/1/sfd.jls")) |> sort
    nsites = df_gz_100.nsites

    darken(color) = RGB(color.r * 0.65, color.g * 0.65, color.b * 0.65)

    p1 = plot_sfd_df_delta(df_gz_100, false, "sfd_temp.png")
    title!(p1, L"GZIP, $10^6$ Steps")
    plot!(p1, ylims=(0.55, 0.9))
    p2 = plot(dpi=1200, size=(600, 300), legend=:bottomright,
        xlabel=L"$R$", ylabel=bitrate_label,
        xticks=8:2:maximum(nsites))
    plot!(nsites, df_xz_1.m1_bits ./ df_xz_1.nsteps, label=L"$\tilde{h}^{(1)}$",
        color=darken(markov1_col), markershape=markov1_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, df_xz_1.m2_bits ./ df_xz_1.nsteps, label=L"$\tilde{h}^{(2)}$",
        color=darken(markov2_col), markershape=markov2_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, df_xz_1.m3_bits ./ df_xz_1.nsteps, label=L"$\tilde{h}^{(3)}$",
        color=darken(other1_col), markershape=other1_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, df_xz_1.true_bits ./ df_xz_1.nsteps, label=L"$\tilde{h}$",
        color=darken(truedyn_col), markershape=truedyn_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(p2, ylims=(0.55, 0.9))
    title!(p2, L"LZMA, $10^8$ Steps")

    gz_corr = df_gz_100.m1_bits ./ df_gz_100.nsteps - df_gz_100.m1_plugin
    xz_corr = df_xz_1.m1_bits ./ df_xz_1.nsteps - df_xz_1.m1_plugin

    p3 = plot(dpi=1200, size=(600, 300), legend=:bottomright,
        xlabel=L"$R$", ylabel=bitrate_label,
        xticks=8:2:maximum(nsites))
    plot!(nsites, df_gz_100.m1_bits ./ df_gz_100.nsteps .- gz_corr, label=L"GZIP $h^{(1)}$",
        color=markov1_col, markershape=markov1_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, df_xz_1.m1_bits ./ df_xz_1.nsteps .- xz_corr, label=L"LZMA $h^{(1)}$",
        color=darken(markov1_col), markershape=markov1_shape, linewidth=0.5,
        markerstrokewidth=0.5)

    plot!(nsites, df_gz_100.true_bits ./ df_gz_100.nsteps .- gz_corr, label=L"GZIP $h$",
        color=truedyn_col, markershape=truedyn_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    plot!(nsites, df_xz_1.true_bits ./ df_xz_1.nsteps .- xz_corr, label=L"LZMA $h$",
        color=darken(truedyn_col), markershape=truedyn_shape, linewidth=0.5,
        markerstrokewidth=0.5)
    title!(p3, "Post-Correction Comparison")

    pmain = plot(p1, p2, p3, layout=(1, 3), size=(1200, 300), dpi=1200,
        legendfontsize=16, xtickfontsize=14, ytickfontsize=14, titlefontsize=20,
        xlabelfontsize=18, ylabelfontsize=18, markersize=7)
    savefig(outfname)
end

function plot_sfd_noisy(data, noiserate)
    subdata = filter(x -> x.noise_rate == noiserate, data)
    subdata = filter(x -> x.nwalkers == 7, subdata)
    subdata = filter(x -> x.kind == "direct", subdata)
    sort!(subdata)
    #println(subdata)

    p1 = plot(legend=:bottomright, title="p = $(noiserate)",
        xlabel=L"R", ylabel=bitrate_label,
        xticks=8:2:maximum(subdata.nsites), legendfontsize=16, xtickfontsize=14,
        ytickfontsize=14, titlefontsize=20, markersize=7, size=(600, 300), dpi=1200)

    #plot!(p1, subdata.nsites, subdata.m0_bits ./ subdata.nsteps, label="Independent")
    m1_bps = subdata.m1_bits ./ subdata.nsteps
    m2_bps = subdata.m2_bits ./ subdata.nsteps
    t_bps = subdata.true_bits ./ subdata.nsteps

    correction = m1_bps - subdata.m1_plugin
    m1_bps -= correction
    m2_bps -= correction
    t_bps -= correction

    plot!(p1, subdata.nsites, m1_bps, label=L"$h^{(1)}$", color=markov1_col, markershape=markov1_shape, linewidth=0.5, markerstrokewidth=0.5)
    plot!(p1, subdata.nsites, m2_bps, label=L"$h^{(2)}$", color=markov2_col, markershape=markov2_shape, linewidth=0.5, markerstrokewidth=0.5)
    plot!(p1, subdata.nsites, t_bps, label=L"$h$", color=truedyn_col, markershape=truedyn_shape, linewidth=0.5, markerstrokewidth=0.5)
    return p1
end

function plot_sfd_noise_quad(outfname)
    sfd_data = deserialize("data/bitrates/1/sfd_noisy.jls")
    plts = [plot_sfd_noisy(sfd_data, nr) for nr in [1e-4, 1e-3, 1e-2, 1e-1]]
    plot(plts..., layout=(2, 2), size=(1200, 600), dpi=1200)
    savefig(outfname)
end

function plot_sfd_method_single(outfname)
    df_gz_1 = filter(x -> x.kind == "delta" && x.nwalkers == 7 && x.particle_id == 1, deserialize("data/gzip_bitrates/1/sfd.jls")) |> sort
    df_gz_100 = filter(x -> x.kind == "delta" && x.nwalkers == 7 && x.particle_id == 1, deserialize("data/gzip_bitrates/100/sfd.jls")) |> sort
    df_xz_1 = filter(x -> x.kind == "delta" && x.nwalkers == 7 && x.particle_id == 1, deserialize("data/bitrates/1/sfd.jls")) |> sort
    df_xz_100 = filter(x -> x.kind == "delta" && x.nwalkers == 7 && x.particle_id == 1, deserialize("data/bitrates/100/sfd.jls")) |> sort

    p1 = plot(legend=:bottomright, title="Comparison of corrected true entropy",
        xlabel=L"R", ylabel=bitrate_label,
        xticks=8:2:maximum(df_gz_1.nsites), legendfontsize=16, xtickfontsize=14,
        ytickfontsize=14, titlefontsize=20, markersize=7, size=(800, 400), dpi=1200)

    gz1_corr = df_gz_1.m1_bits ./ df_gz_1.nsteps .- df_gz_1.m1_plugin
    gz100_corr = df_gz_100.m1_bits ./ df_gz_100.nsteps .- df_gz_100.m1_plugin
    xz1_corr = df_xz_1.m1_bits ./ df_xz_1.nsteps .- df_xz_1.m1_plugin
    xz100_corr = df_xz_100.m1_bits ./ df_xz_100.nsteps .- df_xz_100.m1_plugin

    plot!(p1, df_gz_1.nsites, df_gz_1.true_bits ./ df_gz_1.nsteps - gz1_corr, label="h GZIP, 1e8 steps")
    plot!(p1, df_xz_1.nsites, df_xz_1.true_bits ./ df_xz_1.nsteps - xz1_corr, label="h LZMA, 1e8 steps")
    plot!(p1, df_xz_100.nsites, df_xz_100.true_bits ./ df_xz_100.nsteps - xz100_corr, label="h LZMA, 1e6 steps")
    plot!(p1, df_gz_100.nsites, df_gz_100.true_bits ./ df_gz_100.nsteps - gz100_corr, label="h GZIP, 1e6 steps")

    savefig(outfname)
end


function plot_twotrack_bad_correction(maindf, outfname)
    kSlow = maindf[1, :kslow]
    kFast = maindf[1, :kfast]

    df = filter(x -> x.kfast == kFast && x.kslow == kSlow, maindf) |> sort

    true_bps = df.true_bits ./ df.nsteps
    m1_bps = df.m0_bits ./ df.nsteps

    plot_ylims = (0.008, 0.013)

    # Here, we dodge a bit, and use the max-switchrate plugin estimator in place
    # of the true markov estimator to avoid statistical issues
    delta_bps = true_bps - m1_bps + df.m1_plugin

    p1 = plot(xaxis=:log, legend_position=:bottomright,
        xlabel=L"\log (\gamma / k_s)", ylabel=bitrate_label,
        xlim=(1e-4, 100), ylim=plot_ylims,
        dpi=1200, size=(600, 350), legendfontsize=13, xlabelfontsize=15,
        ylabelfontsize=15, xtickfontsize=13, ytickfontsize=13)

    scatter!(p1, df.kswitch ./ kSlow, delta_bps, markershape=:star, color=:magenta,
        markerstrokewidth=0.5, markersize=10, label=L"h")

    savefig(p1, outfname)
    return p1
end

if abspath(PROGRAM_FILE) == @__FILE__
    targetdir = 1

    cgwalk_df = deserialize("data/bitrates/$(targetdir)/cgwalk.jls")
    saw_df = deserialize("data/bitrates/$(targetdir)/saw.jls")
    sfd_df = deserialize("data/bitrates/$(targetdir)/sfd.jls")
    twotrack_df = deserialize("data/bitrates/$(targetdir)/twotrack.jls")

    sfd_direct_df = filter(x -> x.kind == "direct", sfd_df)
    sfd_delta_df = filter(x -> x.kind == "delta", sfd_df)

    plot_sfd_noise_quad("noisy_sfd.svg")
    plot_cgwalk_df(cgwalk_df, "cgwalk.svg")
    plot_saw_df(saw_df, "saw.svg")
    plot_sfd_dd_encoding_quad(sfd_direct_df, sfd_delta_df, "sfd_quad.svg")

    plot_twotrack_bad_correction(twotrack_df, "tt_bad_correction.svg")

    if isfile("data/gzip_bitrates/1/sfd.jls")
        plot_sfd_data_gzip_quad("gzip_and_data.svg")
        plot_sfd_data_gzip_trirow("gzip_trirow.svg")
    else
        print("No GZIP data present--skipping gzip quad")
    end

    plot_sfd_method_single("test.svg")
end