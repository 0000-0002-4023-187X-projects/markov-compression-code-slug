This repository contains the simulation and analysis code for https://www.biorxiv.org/content/10.1101/2022.01.13.476256v1

### Requirements/Installation

You need to have Julia 1.7 or later installed. 
Downloads can be found [on the Julia website](https://julialang.org/downloads/)

You will also need a command-line implementation of `xz` to be installed and
accessible on the `PATH`.

This is the only setup required: the commands below will automatically download
and install prerequisites. The software has only been tested on Linux under 
Juila 1.7.1, but should, in principle, work on newer versions and other platforms.

### Data directories

There are four directories in this repository, corresponding to the four
systems explored in the paper:

  - Coarse-grained random walk
  - Self-avoiding walk
  - Single-file diffusion on a ring
  - Two-track diffusion model

Each directory contains various files used for generating and plotting data.
If you only want to generate the data and plots, you will only need to worry
about the `generate_data.jl` and `generate_plots.ipynb` files.

### Running Simulations

To run the simulations, take note of what directory the `Project.toml` file is 
in (there should only be one such file in this repo). This is the "project
directory". Then, navigate to the simulation directory you want and run the 
following command:

```
julia --project=<path-to-project-directory> generate_data.jl
```

This will run and generate the appropriate data files.

At the top of each generate_data.jl file are parameters which control how many
points to use. The runtime and memory usage of the simulations scale roughly
linearly with the number of points--each file will have a commented out line
showing how many points were used in the paper, and another line with a set
of reasonable defaults if you want to just quickly run the code.

The memory usage scales with the number of points. As a general rule, most simulations
require about 10*N bytes of memory per thread, so if running with 100m points,
you will need 1GB of memory per thread.

If you wish to do the simulations with thread-parallelism, you can launch julia
with the `--threads=N` flag or by setting `JULIA_NUM_THREADS=N` in the environment.
However, note that the memory required scales linearly with the number of points.

The expected running time for 8 threads, assuming sufficient memory, should be
about 3.5 hours.

### Generating Plots

To generate the plots, you will need to run the Jupyter notebook contained in
each directory. You can do this with the following command:

```
julia --project=<path-to-project-directory> -e "using IJulia; notebook(dir=pwd())"
```

The limits of plots are made assuming the default number of points have been used.
You will need to change them for lower sampling counts.

### Data Generation Demo

In order to quickly generate the data, modify the `NUM_THREADS` variable at the
top of `generate_all_data.sh` and then run that file with bash.

### Reproduction Parameters

To reproduce the results in the paper, go to each `generate_data.jl` file and
replace the commented out parameters with the ones from the paper. Each parameter
will be labeled with a comment describing whether that is the default value
or the value used in the paper.